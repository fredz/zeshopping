var product100 = {
	id: "100",
	name:"Drone 100",
	description: "Drone color white 2.4 GZH fly around height 20m ",
	price: 450,
	money: "Bs",
	image:"image/n1.jpg",
	color: "White, Black",
	width: "200",
	height: "300",
	images: [
		{id: "1", source:"image/n1.jpg", title:"image 1"},
		{id: "2", source:"image/n1.jpg", title:"image 2"},
		{id: "3", source:"image/n1.jpg", title:"image 3"},
	]
}

var productImage = []

var product101 = {
	id: "101",
	name:"Drone 101",
	description: "Drone color white 2.4 GZH fly around height 20m ",
	price: 460,
	money: "Bs",
	image:"image/n1.jpg",
	images: [
		{id: "1", source:"image/n1.jpg", title:"image 1"},
		{id: "2", source:"image/n1.jpg", title:"image 2"},
		{id: "3", source:"image/n1.jpg", title:"image 3"},
	]	
}

var product102 = {
	id: "102",
	name:"Drone 102",
	description: "Drone color white 2.4 GZH fly around height 20m ",
	price: 470,
	money: "Bs",
	image:"image/n1.jpg",
	images: [
		{id: "1", source:"image/n1.jpg", title:"image 1"},
		{id: "2", source:"image/n1.jpg", title:"image 2"},
		{id: "3", source:"image/n1.jpg", title:"image 3"},
	]	
}

var product103 = {
	id: "103",
	name:"Drone 103",
	description: "Drone color white 2.4 GZH fly around height 20m ",
	price: 480,
	money: "Bs",
	image:"image/n1.jpg",
	images: [
		{id: "1", source:"image/n1.jpg", title:"image 1"},
		{id: "2", source:"image/n1.jpg", title:"image 2"},
		{id: "3", source:"image/n1.jpg", title:"image 3"},
	]	
}

var listProduct = [
					product100,
					product101,
					product102,
					product103
				  ];


console.log(listProduct);				  

function buildProdTemplate(){
	var template = '<div id="prod-{0}" class="col-xs-6 col-sm-3 product-item">'+ 
	    '<div class="thumbnail">'+ 
	      '<img data-src="holder.js/100%x200" src="{5}" data-holder-rendered="true" >'+ 
	      '<div class="caption">' +
	        '<h3 class="product-name">{1}</h3>'+             
	        '<div class="product-price">'+
	          '<a href="#" class="cart-icon" ><i class="fa fa-shopping-cart"></i></a>'+ 
	          '<a href="#"  > <span class="price">{3}</span> <span class="money">{4}</span></a>'+
	        '</div>'+ 
	      '</div>'+
	      '<div class="container-product-preview">'+
	        '<p>Quick View</p>'+
	      '</div>'
	    '</div>'+
	  '</div>';

	  return template;
}

function templateThumbnail(){
	
	var thumb = '<li id="thumb-{0}" class="list-group-item">'+
					'<img src="{1}" class="img-rounded" alt="{2}" width="100" height="132">'+
				'</li>';
	return thumb;	
}

function getProductById(id){

	var prod;
	var i;
	var prodSelected;
	for (i in listProduct) {

    	prod = listProduct[i];
    	if(id === prod.id){
    		prodSelected = prod;
    	}
    }

    return prodSelected;
}

// First, check if it isn't implemented yet.
if (!String.prototype.format) {
  String.prototype.format = function () {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function (match, number) {
      return typeof args[number] != 'undefined'? args[number]: match;
    });
  };
}
